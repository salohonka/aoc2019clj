(ns day2.puzzle1)

(def data (vec (map read-string (clojure.string/split (slurp "src/day2/data.txt") #","))))
(def fixedData (-> data (assoc 1 12) (assoc 2 2)))

(defn- perform-op
  [nth-op data]
  (let [[opcode op1 op2 op3] (try (subvec data (* nth-op 4) (* (inc nth-op) 4))
                                  (catch IndexOutOfBoundsException _
                                    [99 0 0 0]))]
    (cond
      (= opcode 1) (assoc data op3 (+ (nth data op1) (nth data op2)))
      (= opcode 2) (assoc data op3 (* (nth data op1) (nth data op2)))
      (= opcode 99) nil
      :else "unexpected opcode")))

(defn compute
  [data]
  (loop [n 0, result data]
    (let [value (perform-op n result)]
      (if (nil? value)
        result
        (recur (inc n) value)))))