(ns day2.puzzle2
  (:require [day2.puzzle1 :refer [compute]]))

(def expected 19690720)

(defn find-combo
  [intcode expected]
  (first (for [x (range 100)
               y (range 100)
               :when (= expected (-> intcode (assoc 1 x) (assoc 2 y) compute first))]
           (+ (* 100 x) y))))