(ns day3.puzzle1)

(def wire1 (clojure.string/split (first (clojure.string/split-lines (slurp "src/day3/data.txt"))) #","))
(def wire2 (clojure.string/split (second (clojure.string/split-lines (slurp "src/day3/data.txt"))) #","))

(def fn-map {\R #(assoc % 0 (inc (first %)))
             \L #(assoc % 0 (dec (first %)))
             \D #(assoc % 1 (dec (second %)))
             \U #(assoc % 1 (inc (second %)))})

(defn- steps
  "Generates visited coordinates from instruction and last coordinate"
  [instr last-coord]
  (let [direction (first instr),
        amount (read-string (subs instr 1))]
    (rest (take (inc amount) (iterate (fn-map direction) last-coord)))))

(defn traverse
  "Generates set of coordinates visited from wire spec"
  [wire]
  (rest (reduce
          #(apply conj %1 (steps %2 (last %1)))
          [[0 0]]
          wire)))

(defn find-crossings
  ([wire1 wire2]
  (let [visited1 (traverse wire1)
        visited2 (set (traverse wire2))
        crosses (for [x visited1
                      :when (contains? visited2 x)] x)]
    crosses))
  ([_ visited1 visited2]
  (let [crosses (for [x visited1
                      :when (contains? visited2 x)] x)]
    crosses)))


(defn find-closest-crossing
  [wire1 wire2]
  (apply min (map
               #(+ (Math/abs (first %)) (Math/abs (second %)))
               (find-crossings wire1 wire2))))