(ns day3.puzzle2
  (:require [day3.puzzle1 :refer [find-crossings traverse]]))

(defn find-shortest-crossing-distance
  [wire1 wire2]
  (let [visited1 (traverse wire1)
        visited2 (traverse wire2)
        crossings (find-crossings nil visited1 (set visited2))
        distances (map
                    #(+ (.indexOf visited1 %) (.indexOf visited2 %))
                    crossings)]
    ; inc by 2 to offset [0 0] being excluded from wire paths
    (+ 2 (apply min distances))))