(ns day4.puzzle2)

(defn- has-a-pair?
  [list]
  (loop [digits list
         previous nil
         ; init with 1 for first digit
         chains [1]]
    (let [curr (first digits)]
      (if (empty? digits)
        (some (partial = 2) chains)
        (recur (rest digits) curr (if (= curr previous)
                                    (assoc chains (-> chains count dec) (-> chains last inc))
                                    (conj chains 1)))))))

(defn is-valid?
  [pw]
  (let [digits (map #(Character/digit % 10) (str pw))]
    (and
      (= digits (sort digits))
      (not (apply distinct? digits))
      (has-a-pair? digits))))