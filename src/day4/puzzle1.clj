(ns day4.puzzle1)

(def input [273025 767253])

(defn is-valid?
  [pw]
  (let [digits (map #(Character/digit % 10) (str pw))]
    (and
      (= digits (sort digits))
      (not (apply distinct? digits)))))

(defn calculate-passwords
  [[lower upper] valid?]
  (->> (range lower (inc upper))
       (filter (partial valid?))
       count))