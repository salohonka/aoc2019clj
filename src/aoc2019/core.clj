(ns aoc2019.core
  (:require [day1.puzzle1 :as d1p1]
            [day1.puzzle2 :as d1p2]
            [day2.puzzle1 :as d2p1]
            [day2.puzzle2 :as d2p2]
            [day3.puzzle1 :as d3p1]
            [day3.puzzle2 :as d3p2]
            [day4.puzzle1 :as d4p1]
            [day4.puzzle2 :as d4p2]))

(defn -main
  []
  (let [x (do (println "Input day number") (clojure.edn/read-string (read-line)))]
    (cond
      (= x 1) (str "puzzle 1: " (d1p1/calcFuel d1p1/data)
                   ", puzzle 2: " (d1p2/calcFuel d1p1/data))
      (= x 2) (str "puzzle 1: " (first (d2p1/compute d2p1/fixedData))
                   ", puzzle 2: " (d2p2/find-combo d2p1/data d2p2/expected))
      (= x 3) (str "puzzle 1: " (d3p1/find-closest-crossing d3p1/wire1 d3p1/wire2)
                   ", puzzle 2: " (d3p2/find-shortest-crossing-distance d3p1/wire1 d3p1/wire2))
      (= x 4) (str "puzzle 1: " (d4p1/calculate-passwords d4p1/input d4p1/is-valid?)
                   ", puzzle 2: " (d4p1/calculate-passwords d4p1/input d4p2/is-valid?))
      :else "Invalid option")))