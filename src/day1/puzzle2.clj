(ns day1.puzzle2
  (:require [day1.puzzle1 :refer [sumFuel]]))

(defn sumTotalFuel
  [fuel]
  (->> fuel
       sumFuel
       (iterate sumFuel)
       (take-while pos?)
       (reduce +)))

(defn calcFuel
  [fuels]
  (reduce + (map sumTotalFuel fuels)))