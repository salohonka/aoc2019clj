(ns day1.puzzle1)

(def data
  (map read-string (clojure.string/split-lines
                     (slurp "src/day1/data.txt"))))

(defn sumFuel
  [x]
  (-> x (quot 3) (- 2)))

(defn calcFuel
  [fuels]
  (reduce + (map sumFuel fuels)))